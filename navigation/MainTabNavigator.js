import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator,createAppContainer } from 'react-navigation';
import TabBarIcon from '../components/TabBarIcon';
import AddScreen from '../screens/AddScreen';
import ListeScreen from '../screens/ListeScreen';
import ProfileScreen from '../screens/ProfileScrenn';
import DetailScreen from '../screens/DetailScreen';
import UpdateScreen from '../screens/UpdateScreen';


const AddStack = createStackNavigator({
  Ajouter: AddScreen,
});

AddStack.navigationOptions = {
  tabBarLabel: 'Ajouter',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-add-circle' : 'md-add-circle'}
    />
  ),
};

const ProfileStack = createStackNavigator({
  Profil: ProfileScreen,
});

ProfileStack.navigationOptions = {
  tabBarLabel: 'Profil',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-person' : 'md-person'}
    />
  ),
};

const HomeStack = createStackNavigator({
  Home: ListeScreen,
  Detail : DetailScreen,
  Update: UpdateScreen
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-home${focused ? '' : '-outline'}`
          : 'md-home'
      }
    />
  ),
};

const bottomtabNav = createBottomTabNavigator({
  AddStack,
  HomeStack,
  ProfileStack,
},
{
   initialRouteName: 'HomeStack',
 });

 export default createAppContainer(bottomtabNav);

import React from 'react';
import {View,Text,StyleSheet,Button} from 'react-native';
import {Container,Content,List,ListItem,Icon} from 'native-base'
import { connect } from 'react-redux'
import FBSDK, {LoginManager,AccessToken} from 'react-native-fbsdk'
import {redirectToDetail} from '../api/util'

const mapStateToProps = (state) => {
    return state
}

class ProfilScreen extends React.Component{
    static navigationOptions = {
        title: 'Profil',
    };
    constructor(props){
        super(props);
        this._logOut = this._logOut.bind(this)
    }

    componentWillUnmount() {
        clearInterval(this.interval);
      }
  
      componentDidMount(){
        this.interval = setInterval(() => this.setState({ time: Date.now() }), 1000);
        redirectToDetail(this)
      }
    
    _logOut(){
      LoginManager.logOut();
      this.props.navigation.navigate("Auth");
    }

    render(){
        return(
            <Container>
                <Content>
                    <List>
                        <ListItem iconLeft>
                            <Icon name='person' />
                            <Text> {this.props.user.name}</Text>
                        </ListItem>
                        <ListItem iconLeft>
                            <Icon name='mail' />
                            <Text> {this.props.user.email}</Text>
                        </ListItem>
                    </List>
                    
                    <Button title="Se Déconnecter" color="red" onPress={()=>{this._logOut()}}>
                        
                    </Button>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    infoContent :{

    }
});

export default connect(mapStateToProps)(ProfilScreen)
import React, { Component } from 'react';
import { View,StyleSheet, Text,Image,Button,Alert } from 'react-native';
import{Container,Content,Card,CardItem} from 'native-base'
import Loader from '../components/Loader';
import firebase from 'firebase'
import {config} from '../api/firebaseConfig'
import { connect } from 'react-redux'
import {redirectToDetail} from '../api/util'

class DetailScreen extends Component {
    static navigationOptions = {
        title: 'Detail',
    };
    constructor(props) {
    super(props);
    this.state = {
      isLoading:false
    };
  }

  componentWillMount(){
    if (!firebase.apps.length) {
        firebase.initializeApp(config);
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  componentDidMount(){
    this.interval = setInterval(() => this.setState({ time: Date.now() }), 1000);
    redirectToDetail(this)
  }

  _deleteItem(){
    this.setState({isLoading:true});
    firebase.database().ref('items/'+this.props.user.id+"/"+this.props.navigation.state.params.item.id).remove()
      .then((result)=>{
        this.props.navigation.navigate("Auth");
        this.setState({isLoading:false});
      })
      .catch((error)=>{
        alert("Une erreur est survenue lors de la suppression de l'item");
        this.setState({isLoading:false});
      })
  }

  _confirmDeleteItem(){
    Alert.alert(
      'Confirmation',
      'Voulez vous vraiment supprimer cet Item ?',
      [
        {text: 'Oui', onPress: () =>  this._deleteItem()},
        {
          text: 'Non',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        }
      ],
      {cancelable: true},
    );
  }

  _updateItem(){
    this.props.navigation.navigate("Update",{"item":this.props.navigation.state.params.item});
  }

  render() {
    return (
      <Container>
          <Content>
              <Card>
                  <CardItem cardBody> 
                    <View style={styles.imageContainer} >
                      <Image style={styles.image}  source={{uri:this.props.navigation.state.params.item.image}} /> 
                    </View>
                  </CardItem>
                  <CardItem >      
                      <Text>{this.props.navigation.state.params.item.description}</Text>
                  </CardItem>
              </Card>
              <View>
                <Button title="Modifier" style={styles.button} onPress={()=>{this._updateItem()}} />
                <Text></Text>
                <Button title="Supprimer" style={styles.button}  color="red" onPress={()=>{this._confirmDeleteItem()}} />
              </View>
          </Content>
          <Loader isLoading={this.state.isLoading} />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  button:{
    marginTop:5
  },
  image: {
    width: 150,
    height: 180,
    margin: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'gray'
  },
  imageContainer:{
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
  }
})
const mapStateToProps = (state) => {
  return state
}
export default connect(mapStateToProps)(DetailScreen)

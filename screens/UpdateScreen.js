import React, { Component } from 'react';
import {config} from '../api/firebaseConfig'
import firebase from 'firebase'
import { connect } from 'react-redux'
import {Container,Content,InputGroup,Icon} from 'native-base';
import {View,Button,Text,Image,KeyboardAvoidingView} from 'react-native';
import { TextInput,StyleSheet,Platform } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob'
import Loader from '../components/Loader';
import {redirectToDetail} from '../api/util'

const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob
const Fetch = RNFetchBlob.polyfill.Fetch
// replace built-in fetch
window.fetch = new Fetch({
    auto : true,
    binaryContentTypes : [
        'image/',
        'video/',
        'audio/',
        'foo/',
    ]
}).build()

class UpdateScreen extends Component {
    static navigationOptions = {
        title: 'Modification Item',
    };
    constructor(props) {
      super(props);
      this.state = {
        imgUri: this.props.navigation.state.params.item.image,
        isLoading : false,
      }
      this.item = this.props.navigation.state.params.item
      this.description = this.item.description
      this.textInput = null
      this.imgUri = this.item.image
      this._uploadImage = this._uploadImage.bind(this)
      this._addImage = this._addImage.bind(this)
    }

    componentWillMount(){
      if (!firebase.apps.length) {
          firebase.initializeApp(config);
      }
    }

    componentWillUnmount() {
      clearInterval(this.interval);
    }

    componentDidMount(){
      this.interval = setInterval(() => this.setState({ time: Date.now() }), 1000);
      redirectToDetail(this)
    }

    _uploadImage(uri,imgName, mime = 'application/octet-stream') {
      return new Promise((resolve, reject) => {
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri
        let uploadBlob = null
  
        const imageRef = firebase.storage().ref('images').child(this.props.user.id+"/"+imgName+".jpg")
  
        fs.readFile(uploadUri, 'base64')
          .then((data) => {
            return Blob.build(data, { type: `${mime};BASE64` })
          })
          .then((blob) => {
            uploadBlob = blob
            return imageRef.put(blob, { contentType: mime })
          })
          .then(() => {
            uploadBlob.close()
            return imageRef.getDownloadURL()
          })
          .then((url) => {
            resolve(url)
          })
          .catch((error) => {
            reject(error)
        })
      })
    }

    _addImage() {
      ImagePicker.showImagePicker({}, (response) => {
          if (response.didCancel) {
              console.log('L\'utilisateur a annulé')
          }
          else if (response.error) {
               alert("Une erreur est survenue");
          }
          else {
              const image = response.uri;
              this.imgUri = response.uri;
              this.setState({
                  imgUri: response.uri
              })
              
          }
      })
    }

    _modifierItem(){
      this.setState({isLoading:true});
      var desc = this.description;
      var imgStr = this.imgUri;
      if(desc.trim() || imgStr.trim()){
        if(imgStr.trim() && this.imgUri!=this.item.image){
          this._uploadImage(this.imgUri,this.props.user.id)
            .then((url) => { 
              console.log(url); 
              firebase.database().ref('items/'+this.props.user.id+"/"+this.item.id).update(
                {
                    image:url,
                    description:this.description
                }
              ).then((result)=>{
                  var item = this.item;
                  item.image = url;
                  item.description = this.description;
                  this.description="";
                  this.textInput.clear();
                  this.imgUri="";
                  this.props.navigation.navigate("Detail",{"item" : item});
                  this.setState({isLoading:false});
              }).catch((error)=>{console.log(error);this.setState({isLoading:false});})
            })
            .catch((error) => {console.log(error);this.setState({isLoading:false})})
        }else{
          firebase.database().ref('items/'+this.props.user.id+"/"+this.item.id).update(
            {
                image:'',
                description:this.description
            }
          ).then((result)=>{
              
              
              var item = this.item;
              if(this.imgUri==this.item.image){
                item.image = this.item.image;
              }else if(!imgStr.trim()){
                item.image = '';
              }
              item.description = this.description;
              this.description="";
              this.textInput.clear();
              this.imgUri="";
              this.props.navigation.navigate("Detail",{"item" : item});
              this.setState({isLoading:false});
          }).catch((error)=>{console.log(error);this.setState({isLoading:false})})
        }
      }else{
        alert("Veuillez inclure au moins une image ou une description");
        this.setState({isLoading:false});
      }
    }

    _getImageBtnLabel(){
      var imgStr = this.state.imgUri;
      if(imgStr.trim()){
        return "Modifier Image";
      }else{
        return "Ajouter Image";
      }
    }
    render() {
      return (
        <KeyboardAvoidingView style={styles.container} behavior="padding">
            <Text style={styles.text}>Veuillez entrez les informations de votre item :</Text>
              <View style={styles.imageContainer}>
                <Image style={styles.img} source={{uri:this.state.imgUri}} />
                <Button title={this._getImageBtnLabel()} onPress={()=>{this._addImage()}} />
              </View>
            <Icon name='list' style={{color:'#384850'}}/>
                <TextInput 
                    ref={input => { this.textInput = input }}
                    style={styles.textArea}
                    placeholder="Sasir Description ici ..." 
                    multiline={true}
                    numberOfLines={11}
                    onChangeText={(text)=>{this.description = text}}
                    defaultValue={this.description}
                />
            <View style={styles.buttonContainer}>
                <Button  style={styles.buttonStyle} color="green" title="Modifier Item" onPress={()=>{this._modifierItem()}} />
            </View>
            <Loader style={{top:25}} isLoading={this.state.isLoading} />
        </KeyboardAvoidingView>
      );
    }

}

const styles = StyleSheet.create({
  text:{
      margin:20,
      fontWeight: 'bold'
  },
  buttonStyle:{
    
  },
  addStyle:{
      color:'red',
  },
  
  textAreaContainer: {
    borderColor: 'gray',
    borderWidth: 1,
  },
  textArea: {
    height: 150,
    justifyContent: "flex-start"
  },

  img: {
    width: 150,
    height: 180,
    margin: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'gray'
  },
  imageContainer:{
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  buttonContainer:{
    marginBottom:20
  }
})

const mapStateToProps = (state) => {
  return state
}
export default connect(mapStateToProps)(UpdateScreen)


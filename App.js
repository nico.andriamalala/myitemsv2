import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import AppNavigator from './navigation/AppNavigator';
import { Provider } from 'react-redux'
import Store from './store/configureStore'
import NotifHandler from './components/NotifHandler';
import { YellowBox } from 'react-native';

YellowBox.ignoreWarnings(['Setting a timer','source.uri']);

type Props = {};
export default class App extends Component<Props> {

  constructor(props){
    super(props)
    console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed','Setting a timer'];
  }

  render() {
    return (
      <Provider store={Store}>
        <View style={styles.container}>
            <NotifHandler />
            {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
            <AppNavigator />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

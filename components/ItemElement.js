import React, { Component } from 'react';
import {Image,StyleSheet,TouchableOpacity,View,Text} from 'react-native';

export default class ItemElement extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <View>
            <TouchableOpacity onPress={()=>{this.props.detailItem(this.props.item)}}>
                <View style={styles.main_container}>
                    <Image style={styles.image} source={{uri:this.props.item.image}} /> 
                    <View style={styles.content_container}>
                        <View style={styles.description_container}>
                            <Text style={styles.description_text} numberOfLines={6}>{this.props.item.description}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    );
  }
}
const styles = StyleSheet.create({
    main_container: {
      height: 190,
      flexDirection: 'row',

      margin:2,
    },
    image: {
      width: 150,
      height: 180,
      margin: 5,
      backgroundColor: 'gray'
    },
    content_container: {
      flex: 1,
      margin: 5
    },
    description_container: {
      flex: 7
    },
    description_text: {
      fontStyle: 'italic',
      color: '#666666'
    },
  })
  
import React, { Component } from 'react';
import { View,ActivityIndicator,StyleSheet } from 'react-native';

export default class Loader extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
      if(this.props.isLoading){
        return (
            <View style={styles.loading_container}>
              <ActivityIndicator size='large' />
            </View>
        );
      }else{
          return(<View></View>);
      }
  }
}

const styles = StyleSheet.create({
    loading_container: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 100,
      bottom: 0,
      alignItems: 'center',
      justifyContent: 'center',
      
    }
  })
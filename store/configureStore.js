
import { createStore } from 'redux';
import toggleUserInfo from './reducers/userInfoReducer'

export default createStore(toggleUserInfo)